using System;
using UnityEngine.UIElements;

namespace Quickytools
{
    public partial class UIInputSlider
    {
        private static float GetFloatAttribute(
            IUxmlAttributes bag,
            string attrName,
            float defaultValue = 0
        )
        {
            if (bag.TryGetAttributeValue(attrName, out var sValue))
            {
                if (float.TryParse(sValue, out var result))
                {
                    return result;
                }
                else
                {
                    throw new ArgumentException($"{attrName} has an invalid value of {sValue}");
                }
            }
            return defaultValue;
        }

        private static int GetIntAttribute(
            IUxmlAttributes bag,
            string attrName,
            int defaultValue = 0
        )
        {
            if (bag.TryGetAttributeValue(attrName, out var sValue))
            {
                if (int.TryParse(sValue, out var result))
                {
                    return result;
                }
                else
                {
                    throw new ArgumentException($"{attrName} has an invalid value of {sValue}");
                }
            }
            return defaultValue;
        }
    }
}