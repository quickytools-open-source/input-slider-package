using System;
using UnityEngine;
using UnityEngine.UIElements;

namespace Quickytools
{
    /// <summary>
    /// Slider with input
    /// </summary>
    public partial class UIInputSlider : VisualElement
    {
        private const string InputSliderClass = "input-slider";
        private const string SliderValueTextClass = "type-slider-value";
        private const string SliderValueTextName = "slider-text";
        private const string SliderValueValueName = "slider-value";

        public Action<string, float> OnChangeEvent;

        private TextField valueInput;
        private Slider slider;

        private InitialAttributes initialValues;

        private float _groundTruth;

        public float Value => _groundTruth;

        private float CappedSliderValue => Mathf.Max(Mathf.Min(initialValues.sliderMax, Value), initialValues.sliderMin);

        private string lastDisplayValue = null;

        private bool isSliderInternalChange = false;
        private bool isSyncInternalChange = false;

        private float DisplayValue
        {
            set
            {
                var s = value.ToString(initialValues.DecimalFormat);
                s = string.IsNullOrEmpty(s) ? "0" : s;
                // Unregister/register doesn't always enclose changes (especially on out of bounds correction). Cache value for comparison later and determine how change was made.
                lastDisplayValue = s;
                valueInput.value = s;
            }
        }

        public UIInputSlider()
        {
            RegisterCallback<GeometryChangedEvent>(OnGeometryChange);
        }

        private void OnGeometryChange(GeometryChangedEvent e)
        {
            UnregisterCallback<GeometryChangedEvent>(OnGeometryChange);

            if (childCount == 0)
            {
                InitializeStateUi();
            }
        }

        protected virtual void InitializeStateUi()
        {
            _groundTruth = initialValues.value;

            AddToClassList(InputSliderClass);

            // TODO Revisit layout and likely make two row w/ spacing
            if (!string.IsNullOrEmpty(initialValues.text))
            {
                var textLabel = new Label
                {
                    text = initialValues.text,
                    name = SliderValueTextName
                };
                textLabel.AddToClassList(SliderValueTextClass);
                Add(textLabel);
            }

            valueInput = new TextField
            {
                name = SliderValueValueName
            };
            valueInput.AddToClassList(SliderValueTextClass);
            Add(valueInput);
            valueInput.RegisterCallback<ChangeEvent<string>>(InputTextChanged);
            valueInput.RegisterCallback<FocusOutEvent>(InputTextFocusOut);

            slider = new Slider
            {
                value = CappedSliderValue,
                lowValue = initialValues.sliderMin,
                highValue = initialValues.sliderMax
            };
            slider.value = CappedSliderValue;
            Add(slider);
            slider.RegisterCallback<ChangeEvent<float>>(SliderValueChanged);

            DisplayValue = initialValues.value;

            SendChangeEvent();
        }

        // Keep separate from Value set property since can result in infinite loop if changes are made without understanding the entire structure of this class
        public void SyncState(float value)
        {
            _groundTruth = value;
            isSyncInternalChange = true;
            valueInput.value = value.ToString(initialValues.DecimalFormat);
        }

        // Sets text value without syncing slider state
        public void UpdateTextValue(float value)
        {
            _groundTruth = value;
            DisplayValue = _groundTruth;
        }

        private void InputTextFocusOut(FocusOutEvent e)
        {
            UpdateInputText();
        }

        private void InputTextChanged(ChangeEvent<string> e)
        {
            UpdateInputText(true);
        }

        private void UpdateInputText(bool isFocused = false)
        {
            if (isSyncInternalChange)
            {
                isSyncInternalChange = false;
                SetSliderValueInternal();
                return;
            }

            var valueText = valueInput.value;
            if (float.TryParse(valueText, out var f))
            {
                // State should be valid if text is the last display value set
                if (valueText == lastDisplayValue)
                {
                    return;
                }

                f = Mathf.Min(Mathf.Max(initialValues.min, f), initialValues.max);
                _groundTruth = f;

                SetSliderValueInternal();

                SendChangeEvent();
            }
            else
            {
                if ((string.IsNullOrWhiteSpace(valueInput.value) ||
                    valueText == ".") &&
                    isFocused
                   )
                {
                    return;
                }

                DisplayValue = _groundTruth;
            }
        }

        // Slider register/unregister doesn't happen immediately. Check for true change in change callback.
        private void SetSliderValueInternal()
        {
            isSliderInternalChange = true;
            slider.value = CappedSliderValue;
        }

        private void SliderValueChanged(ChangeEvent<float> e)
        {
            // Slider unregister, change, register isn't synchronous as expected.
            if (isSliderInternalChange)
            {
                isSliderInternalChange = false;
                return;
            }

            UpdateTextValue(e.newValue);
            SendChangeEvent();
        }

        protected virtual void SendChangeEvent()
        {
            OnChangeEvent?.Invoke(name, _groundTruth);
        }
    }
}
