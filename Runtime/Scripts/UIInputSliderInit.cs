using System;
using UnityEngine;
using UnityEngine.UIElements;

namespace Quickytools
{
    public partial class UIInputSlider
    {
        public new class UxmlFactory : UxmlFactory<UIInputSlider, UxmlTraits> { }

        public new class UxmlTraits : VisualElement.UxmlTraits
        {
            private const string AttrMinValue = "min-value";
            private const string AttrMaxValue = "max-value";
            private const string AttrSliderMinValue = "slider-min-value";
            private const string AttrSliderMaxValue = "slider-max-value";
            private const string AttrValue = "value";
            private const string AttrText = "text";
            private const string AttrDecimalCount = "decimal-count";

            public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
            {
                base.Init(ve, bag, cc);

                // Value range. Input can assume all values in this range.
                var minValue = GetFloatAttribute(bag, AttrMinValue, float.MinValue);
                var maxValue = GetFloatAttribute(bag, AttrMaxValue, float.MaxValue);
                if (maxValue <= minValue)
                {
                    throw new ArgumentException($"Max value {maxValue} must be greater than {minValue}.");
                }

                // Slider range. Can differ from value range. Use to nudge values in a certain window.
                var sliderMinValue = GetFloatAttribute(bag, AttrSliderMinValue, minValue == float.MinValue ? 0 : minValue);
                var sliderMaxValue = GetFloatAttribute(bag, AttrSliderMaxValue, maxValue == float.MaxValue ? 1 : maxValue);
                if (sliderMaxValue <= sliderMinValue)
                {
                    throw new ArgumentException($"Slider max value {sliderMaxValue} must be greater than {sliderMinValue}.");
                }
                if (sliderMaxValue > maxValue)
                {
                    sliderMaxValue = maxValue;
                }
                if (sliderMinValue < minValue)
                {
                    sliderMinValue = minValue;
                }

                var value = GetFloatAttribute(bag, AttrValue, float.MaxValue);
                if (value == float.MaxValue)
                {
                    value = (sliderMinValue + sliderMaxValue) / 2;
                    if (sliderMinValue == float.MinValue ^ sliderMaxValue == float.MaxValue)
                    {
                        value = sliderMinValue == float.MinValue ? sliderMaxValue : sliderMinValue;
                    }
                }
                if (value < minValue || value > maxValue)
                {
                    throw new ArgumentException($"Value {value} must be between {minValue}-{maxValue}.");
                }

                bag.TryGetAttributeValue(AttrText, out var text);

                bool isZeroOneRange = Mathf.Approximately(0, sliderMinValue) &&
                                      Mathf.Approximately(1, sliderMaxValue);

                var decimalCount = GetIntAttribute(bag, AttrDecimalCount, isZeroOneRange ? 2 : 0);

                var sliderValue = ve as UIInputSlider;
                sliderValue.initialValues = new InitialAttributes
                {
                    min = minValue,
                    max = maxValue,
                    range = maxValue - minValue,
                    sliderMin = sliderMinValue,
                    sliderMax = sliderMaxValue,
                    sliderRange = sliderMaxValue - sliderMinValue,
                    value = value,
                    text = text,
                    decimalCount = decimalCount
                };
            }
        }
    }

    struct InitialAttributes
    {
        public float min;
        public float max;
        public float range;
        public float sliderMin;
        public float sliderMax;
        public float sliderRange;
        public float value;
        public string text;
        public int decimalCount;

        private string decimalFormat;
        public string DecimalFormat
        {
            get
            {
                if (string.IsNullOrWhiteSpace(decimalFormat))
                {
                    decimalFormat = "#." + new string('#', Mathf.Max(0, decimalCount));
                }
                return decimalFormat;
            }
        }

        public override string ToString()
        {
            return $"{text} {min}-{max}. Slider {sliderMin}-{sliderMax}. {value}";
        }
    }
}
