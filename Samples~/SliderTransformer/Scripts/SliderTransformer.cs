using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Quickytools;

[RequireComponent(typeof(UIDocument))]
public class SliderTransformer : MonoBehaviour
{
    [SerializeField]
    Transform targetObject = default;

    private IEnumerable<UIInputSlider> sliderElements = null;

    private void OnEnable()
    {
        if (sliderElements == null)
        {
            var document = GetComponent<UIDocument>().rootVisualElement;
            sliderElements = document.Query<UIInputSlider>().ToList();
        }

        foreach (var slider in sliderElements)
        {
            slider.OnChangeEvent += OnSliderChange;
        }
    }

    private void OnDisable()
    {
        foreach (var slider in sliderElements)
        {
            slider.OnChangeEvent -= OnSliderChange;
        }
    }

    private void OnSliderChange(string name, float value)
    {
        var scale = targetObject.localScale;
        var position = targetObject.position;

        switch (name)
        {
            case "width":
                scale.x = value;
                break;
            case "depth":
                scale.z = value;
                break;
            case "height":
                scale.y = value;
                break;
            case "x":
                position.x = value;
                break;
            case "y":
                position.y = value;
                break;
            case "z":
                position.z = value;
                break;
        }

        targetObject.localScale = scale;
        targetObject.position = position;
    }
}
